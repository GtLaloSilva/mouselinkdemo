package com.tegap.mouselink.app.values;

/**
 * Created by Lalo on 22/03/15.
 * Disconnect command.
 * No params are needed so far.
 */
public class Disconnect extends Command {

    public Disconnect() {
        super(1);
    }

}
