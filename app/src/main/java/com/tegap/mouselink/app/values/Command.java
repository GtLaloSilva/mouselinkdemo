package com.tegap.mouselink.app.values;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Lalo on 22/03/15.
 * Generic command class.
 */
public class Command {

    /**
     * Params to be used in command.
     */
    protected Map<String, Object> params;

    /**
     * Main constructor for command.
     * @param numParams Required number of params to be used.
     */
    public Command(int numParams){
        params = new HashMap<>(numParams);
    }

}
