package com.tegap.mouselink.app.values;

/**
 * Created by Lalo on 22/03/15.
 */
public class MovePointer extends Command{

    public static final String PARAM_X = "x";
    public static final String PARAM_Y = "y";

    /**
     * Main constructor for command.
     *
     */
    public MovePointer(float x, float y) {
        super(2);
        setX(x);
        setY(y);
    }

    public void setX(float x){
        params.put(PARAM_X, x);
    }

    public void setY(float y){
        params.put(PARAM_Y, y);
    }

    public float getX(){
        return (float)params.get(PARAM_X);
    }

    public float getY(){
        return (float)params.get(PARAM_Y);
    }

    @Override
    public String toString() {
        return params.get(PARAM_X) + "," + params.get(PARAM_Y) + "\n";
    }
}
