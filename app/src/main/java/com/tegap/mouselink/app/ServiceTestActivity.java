package com.tegap.mouselink.app;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.tegap.mouselink.app.values.Command;
import com.tegap.mouselink.app.values.Connect;
import com.tegap.mouselink.app.values.Disconnect;
import com.tegap.mouselink.app.values.MovePointer;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;


public class ServiceTestActivity extends ActionBarActivity {

    /* ============ ATTRIBUTES ============ */

    private static final int defaultPort = 1234;

    // IP input.
    private EditText etIP = null;
    // Coordinates inputs.
    private EditText etX = null;
    private EditText etY = null;

    // Button to control connection
    private Button btnConnectDisconnect;

    private Button btnSend;

    // TAG for debugging
    private static final String TAG = "ServiceTestActivity";

    private SocketThread socketThread;

    // Indicates if we are connected to server
    private boolean socketConnected = false;

    /* ============ MSGs DEFs ============ */
    /* Definition of thread-activity messages interaction */
    public final int MSG_TYPE_CONNECT = 1;
    public final int MSG_TYPE_DISCONNECT = 2;
    public final int MSG_TYPE_SEND_COMMAND = 3;

    public final int MSG_TYPE_ERROR_CONNECT = 11;
    public final int MSG_TYPE_ERROR_DISCONNECT = 12;
    public final int MSG_TYPE_ERROR_SEND = 13;

    public final int MSG_TYPE_SUCCESS_CONNECT = 21;
    public final int MSG_TYPE_SUCCESS_DISCONNECT = 22;
    public final int MSG_TYPE_SUCCESS_SEND = 23;

    public final int MSG_TYPE_READY = 31;

    /* ============ UI MSG HANDLER ============ */

    public Handler mHandler;

    /* ============ LIFECYCLE – Activity ============ */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_test);

        etIP = (EditText)findViewById(R.id.etIP);
        etX = (EditText)findViewById(R.id.etX);
        etY = (EditText)findViewById(R.id.etY);
        btnConnectDisconnect = (Button)findViewById(R.id.btnConnectDisconnect);
        btnSend = (Button)findViewById(R.id.btnSend);

        // Instantiate msg handler.
        mHandler = new Handler(){
            @Override
            public void handleMessage(Message msg) {

                String error;

                Context context = getApplicationContext();
                CharSequence text = "";
                int duration = Toast.LENGTH_SHORT;

                int btnResId = R.string.label_btn_connect;

                switch (msg.what){
                    case MSG_TYPE_ERROR_CONNECT:

                        error = (String)msg.obj;

                        text = getString(R.string.label_error_connect) + error;

                        socketConnected = false;

                        break;

                    case MSG_TYPE_ERROR_DISCONNECT:

                        error = (String)msg.obj;

                        text = getString(R.string.label_error_disconnect) + error;

                        socketConnected = false;

                        break;

                    case MSG_TYPE_SUCCESS_CONNECT:

                        btnResId = R.string.label_btn_disconnect;

                        socketConnected  = true;

                        text = getString(R.string.label_success_connect);

                        break;

                    case MSG_TYPE_SUCCESS_DISCONNECT:

                        socketConnected = false;

                        text = getString(R.string.label_success_disconnect);

                        break;

                    case MSG_TYPE_SUCCESS_SEND:

                        text = "Coordenadas enviadas :)";
                        btnResId = R.string.label_btn_disconnect;

                        break;
                    case MSG_TYPE_ERROR_SEND:

                        text = "Error al enviar las coordenadas :(";

                        break;
                    case MSG_TYPE_READY:

                        String ip = etIP.getText().toString();

                        if(!socketConnected && ip != null && !ip.equals("")){
                            onConnectDisconnectClicked(btnConnectDisconnect);
                        }

                        break;
                }

                if(msg.what != MSG_TYPE_SUCCESS_SEND && msg.what != MSG_TYPE_ERROR_SEND){
                    btnConnectDisconnect.setText(getString(btnResId));
                    btnConnectDisconnect.setEnabled(true);
                    etIP.setEnabled(!socketConnected);
                    ServiceTestActivity.this.enableCoordinatesFields(socketConnected);
                }

                if(msg.what == MSG_TYPE_SUCCESS_CONNECT ||
                        msg.what == MSG_TYPE_ERROR_CONNECT ||
                        msg.what == MSG_TYPE_ERROR_SEND ||
                        msg.what == MSG_TYPE_ERROR_DISCONNECT){
                    Toast.makeText(context, text, duration).show();
                }
            }
        };

        // Initial creation of socket thread.
        socketThread = new SocketThread(mHandler);

    }

    private void enableCoordinatesFields(boolean socketConnected) {
        etX.setEnabled(socketConnected);
        etY.setEnabled(socketConnected);
        btnSend.setEnabled(socketConnected);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "Calling onStart");
        // Create socket if needed
        if(socketThread == null){
            socketThread = new SocketThread(mHandler);
        }
        // Start the socket thread.
        socketThread.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "Calling onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "Calling onPause ");
        if(socketConnected){
            onConnectDisconnectClicked(btnConnectDisconnect);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "Calling onStop");
        // Stop socket thread.
        socketThread.mSocketHandler.getLooper().quit();
        socketThread.interrupt();
        socketThread = null;
    }

    /* ============ MENU CREATED ============ */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_service_test, menu);
        return true;
    }

    /* ============ MENU ITEM SELECTION ============ */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /* ============ UI method handlers ============ */

    public void onConnectDisconnectClicked(View view) {



        if(!socketConnected){

            // Get IP
            String ip = etIP.getText().toString();

            Connect cmdConnect = new Connect(ip, defaultPort);

            sendMessageToThread(MSG_TYPE_CONNECT, cmdConnect);

            btnConnectDisconnect.setText(getString(R.string.label_btn_connecting));
            btnConnectDisconnect.setEnabled(false);

            etIP.setEnabled(false);

        }else{

            Disconnect cmdDisconnect = new Disconnect();

            sendMessageToThread(MSG_TYPE_DISCONNECT, cmdDisconnect);

            btnConnectDisconnect.setText(getString(R.string.label_btn_disconnecting));
            btnConnectDisconnect.setEnabled(false);

        }
    }

    public void onSendClicked(View view) {
        // Get coordinates

        float x = Float.valueOf(etX.getText().toString());
        float y = Float.valueOf(etY.getText().toString());

        MovePointer cmdMovePointer = new MovePointer(x, y);

        sendMessageToThread(MSG_TYPE_SEND_COMMAND, cmdMovePointer);
    }

    /* ============ UTILITY METHODS ============ */

    public void sendMessageToThread(int what, Command command){
        Message msg;
        msg = Message.obtain();
        msg.what = what;
        msg.obj = command;

        socketThread.mSocketHandler.sendMessage(msg);
    }

    /* ============ SOCKET THREAD ============ */
    /**
     * Main socket to send commands to server
     */
    public class SocketThread extends Thread {

        // Main socket
        private Socket clientSocket = null;

        // Timeout to connect.
        private static final int sockTimeout = 5000;

        // Socket message handler.
        public Handler mSocketHandler;
        private Handler mActivityHandler;

        // Streams to communicate with remote.
        private InputStream nis;
        private OutputStream nos;

        public SocketThread(Handler mActivityHandler){
            this.mActivityHandler = mActivityHandler;
        }

        @Override
        public void run() {
            // Start looper
            Looper.prepare();

            mSocketHandler = new Handler(){
                @Override
                public void handleMessage(Message msg){

                    // Decode message
                    switch (msg.what){

                        case MSG_TYPE_CONNECT:

                            Connect connect = (Connect)msg.obj;

                            InetSocketAddress socketAddress = new InetSocketAddress(connect.getIP(),
                                                                                    connect.getPort());
                            clientSocket = new Socket();

                            try {

                                clientSocket.connect(socketAddress, sockTimeout);

                                if(clientSocket.isConnected()){
                                    nis = clientSocket.getInputStream();
                                    nos = clientSocket.getOutputStream();

                                    sendMessageToUI(MSG_TYPE_SUCCESS_CONNECT, null);

                                }

                            } catch (IOException e) {
                                e.printStackTrace();

                                sendMessageToUI(MSG_TYPE_ERROR_CONNECT, e.getMessage());

                            }

                            break;

                        case MSG_TYPE_DISCONNECT:

                            try {

                                nis.close();
                                nos.close();
                                clientSocket.close();
                                clientSocket = null;

                                sendMessageToUI(MSG_TYPE_SUCCESS_DISCONNECT, null);

                            } catch (IOException e) {
                                e.printStackTrace();

                                sendMessageToUI(MSG_TYPE_ERROR_DISCONNECT, e.getMessage());

                            }

                            break;

                        case MSG_TYPE_SEND_COMMAND:

                            MovePointer movePointer = (MovePointer)msg.obj;

                            String coordinates = movePointer.toString();

                            try {

                                nos.write(coordinates.getBytes());

                                sendMessageToUI(MSG_TYPE_SUCCESS_SEND, null);

                            } catch (IOException e) {
                                e.printStackTrace();

                                sendMessageToUI(MSG_TYPE_ERROR_SEND, e.getMessage());

                            }

                            break;
                        default:
                            break;
                    }
                }
            };

            sendMessageToUI(MSG_TYPE_READY, null);

            Looper.loop();

            Log.i(TAG, "Looper.loop ended!");

        }

        private void sendMessageToUI(int what, String description){
            Message uiMsg = Message.obtain();
            uiMsg.what = what;
            uiMsg.obj = description;

            mActivityHandler.sendMessage(uiMsg);
        }

    } // SocketThread Ends

}
