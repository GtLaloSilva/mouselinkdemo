package com.tegap.mouselink.app.values;

/**
 * Created by Lalo on 22/03/15.
 * Connect command.
 * Required params are IP and port to connect with server.
 */
public class Connect extends Command {

    public static final String PARAM_IP = "ip";
    public static final String PARAM_PORT = "port";

    public Connect() {
        super(2);
    }

    public Connect(String ip, int port){
        super(2);
        setIP(ip);
        setPort(port);
    }

    public void setIP(String ip){
        params.put(PARAM_IP, ip);
    }

    public void setPort(int port){
        params.put(PARAM_PORT, port);
    }

    public String getIP(){
        return (String)params.get(PARAM_IP);
    }

    public int getPort(){
        return (int)params.get(PARAM_PORT);
    }

}
