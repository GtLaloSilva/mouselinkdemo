package com.tegap.mouselink.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

/**
 * Created by Lalo on 22/03/15.
 */
public class SocketService extends IntentService {

    private SocketAddress socketAddress = null;
    private Socket clientSocket = null;
    private InputStream nis = null;
    private OutputStream nos = null;

    public static final String EXTRA_CMD = "com.tegap.mouselink.EXTRA_CMD";

    public SocketService(){
        super("Default Constructor");
    }

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public SocketService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        String commandString = intent.getStringExtra(EXTRA_CMD);

        /* Service ops definition
           - Connect
           cn|ip|port
           - Disconnect
           dc
           - Command
           cmd|params

        */

        String opData[] = commandString.split(",");

        if(opData[0].equals("cn")){

            if(clientSocket == null){
                socketAddress = new InetSocketAddress(opData[1], Integer.valueOf(opData[2]));
                clientSocket = new Socket();
                try {
                    clientSocket.connect(socketAddress, 5000);
                    if(clientSocket.isConnected()){
                        // Assign streams
                        nis = clientSocket.getInputStream();
                        nos = clientSocket.getOutputStream();
                        // TODO Notify UI
                        Log.i("SocketService", "Cliente conectado");
                    }else{
                        // TODO Notify UI
                        Log.w("SocketService", "No se pudo conectar");
                    }
                } catch (IOException e) {
                    // TODO Notify UI
                    Log.e("SocketService", "Error al conectar");
                    e.printStackTrace();
                }
            }

        }else if(opData[0].equals("dc")){
            try {
                nos.write(commandString.getBytes());
                clientSocket.close();
            } catch (IOException e) {
                // TODO Notify UI
                Log.e("SocketService", "Error al desconectar");
                e.printStackTrace();
            }
        }else if(opData[0].equals("cmd")){
            try {
                nos.write(commandString.getBytes());
                // TODO wait for ack
            } catch (IOException e) {
                // TODO Notify UI
                e.printStackTrace();
            }
        }


    }
}
