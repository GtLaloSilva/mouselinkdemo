# MouseLink #

MouseLink Demo

### What is MouseLink? ###

* Simple Android App to Control your mouse pointer with your Android mobile device.
* 1.0

### Get Started? ###

* Clone the repo
* Open with Android Studio
* Download MouseLink Server code.
* Compile and package app.
* Run the app from Android Studio or create an APK.

### Contribution guidelines ###

* Writing tests (would greatly appreciate).
* Implement missing features, like scrolling.

### Project Creator ###

* Eduardo Gutiérrez Silva. (gt.silva.e at gmail.com).